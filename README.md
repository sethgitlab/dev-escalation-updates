# Dev Escalation Updates

A repo to show the changes made to the dev-escalation code

This repo is not actually deployed. The production code is in the Infra/Dev Escalation On-Call Schedule file directly and may differ from this file.

Google App Scripts does not have an easy integration into version control. This repo was created as a way to communicate changes, since the Google sheet only has the latest version. 
