function ONCALL_FULL_NAME(oncallName) {
  return oncallName.split(",")[0];
}

function cachedTeam() {
  const cache = CacheService.getScriptCache()
  const team = cache.get('team.json');
  if(team != null) {
    return JSON.parse(team);
  }
  
  // Just a proxy for team.yml since I can't parse YML in google apps script: https://gitlab.com/DylanGriffith/oncall-helper/blob/master/app.rb#L11
  const teamJson = UrlFetchApp.fetch('https://dylangriffith-oncall-helper.35.202.232.20.nip.io/team.json');
  
  const data = JSON.parse(teamJson);
  
  // Need smaller payload in order to use cache (due to limits)
  const minimalData = [];
  
  for(i = 0; i < data.length; i++) {
    minimalData.push({
      name: data[i].name,
      country: data[i].country,
    });
  }
  
  cache.put('team.json', JSON.stringify(minimalData), 60);
  
  return minimalData;
}

function ONCALL_COUNTRY(oncallName) {
  const name = ONCALL_FULL_NAME(oncallName);
 
  const people = cachedTeam();
  
  for(i = 0; i < people.length; i++) {
    if(people[i].name === name) {
      return people[i].country;
    }
  }
  
  return 'UNKNOWN';
}

function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Sync to Calendar')
      .addItem('Schedule shifts', 'scheduleShifts')
      .addToUi();
 };

  function scheduleShifts() {
    var spreadsheet = SpreadsheetApp.getActiveSheet();
    var ui = SpreadsheetApp.getUi();
    var calendarId = spreadsheet.getRange("C197").getValue();
    
    var signups = spreadsheet.getRange("A7:F192").getValues();
    
    var eventCal = CalendarApp.getCalendarById(calendarId);

    Logger.log('Looking up calendar "%s".', calendarId);
    
    if (eventCal == null)
    {
      Logger.log("Calendar not found, or insufficient permissions.");
      ui.alert("Calendar not found, or insufficient permissions, check that you have write permission to the calendar ID that is listed in cell C197");
      return;
    }

    Logger.log('eventCal', eventCal == null)
    Logger.log('Found calendar "%s".', eventCal.getName());
  
    var eventsDeleted = 0;
    var eventsAdded = 0;

    var onCallEventOptions = {description: "Thank you for being on-call.\n\nBeing a developer on call is a very important part of ensuring GitLab is delivering a reliable product and service to our customers.\n\nIf this is your first time being on-call or you have questions about the on-call process please read the handbook entry:\n\nhttps://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html"}

    for (x=0; x<signups.length; x++) {

      var shift = signups[x];
      var startHour = new Date(shift[4]);
      var endHour = new Date(shift[5]);
      var volunteer = shift[2];
      var email = shift[3];
      
      /* Don't create a calendar event, unless we have a volunteer */
      if (volunteer == null || volunteer.toString().trim() == "" || startHour === "" ) 
      { 
        console.log('Processing shift:', volunteer, ". Skipping...")
        continue; 
      }
      else 
      {
        console.log('Processing shift:', volunteer)
      }

      var existingEvents = eventCal.getEvents(startHour, endHour);

      if (existingEvents.length > 0) {

        /* If only 1 event exists, the title is the same and the email address has already been invited, then go to the next line */
        if (existingEvents.length == 1 && existingEvents[0].getTitle() == volunteer)
        {
          var gl = existingEvents[0].getGuestList();
          if (gl != null && gl.length == 1 && gl[0].getEmail() == email)
          {
            console.log("Calendar entry for shift found. Skipping...");
            continue;            
          }
        }

        for (y=0; y<existingEvents.length; y++) {
          console.log("Deleting all shifts for this time window.");
          existingEvents[y].deleteEvent();
          eventsDeleted++;
        }
      };

      if (volunteer) {
        console.log("New calendar entry added.");
        var event = eventCal.createEvent(volunteer, startHour, endHour, onCallEventOptions);
        eventsAdded++;
      };        
              
      if (event && email) {
        console.log("Guest added to calendar event %s", email)
        event.addGuest(email);
      };
      if (x % 10 == 0) { Utilities.sleep(3000); }
    };

    ui.alert("Calendar successfully synced. " + eventsDeleted.toString() + " events deleted. " + eventsAdded.toString() +" events added.")

  };
